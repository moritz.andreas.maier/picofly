#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include "BMX055.h"

BMX055 bmx;

void setup(){

Serial.begin(115200);
while (!Serial);
bmx.beginAcc(0x03); // 0x03 is range 2g

}

void loop(){

Serial.print("x: ");
Serial.println(bmx.getAccelerationX());
Serial.print("y: ");
Serial.println(bmx.getAccelerationY());
Serial.print("z: ");
Serial.println(bmx.getAccelerationZ());
delay(100);

}