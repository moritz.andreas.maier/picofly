import machine, neopixel
import time

led_pin = machine.Pin(28)

np = neopixel.NeoPixel(led_pin, 1)

np[0] = (255, 0, 0) # set to red, full brightness
np.write()
time.sleep(3)

np[0] = (0, 255, 0) # set to green, full brightness
np.write()
time.sleep(3)
np[0] = (0, 0, 255) # set to blue, full brightness
np.write()
time.sleep(3)
np[0] = (255, 0, 255) # set to purple, full brightness
np.write()
time.sleep(3)
np[0] = (0, 0, 0) # turn off
np.write()
