from machine import Pin, PWM
#from time import sleep

motor11 = PWM(Pin(0, mode=Pin.OUT))
motor21 = PWM(Pin(2, mode=Pin.OUT))
motor31 = PWM(Pin(6, mode=Pin.OUT))
motor41 = PWM(Pin(8, mode=Pin.OUT))

motors = [motor11, motor21, motor31, motor41]

for motor in motors:
    motor.freq(15600)

# 0 to 100
speed = 0

while True:
    for motor in motors:
        motor.duty_u16(int(speed/100*65536))
