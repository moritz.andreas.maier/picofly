#coding:utf-8
import math
import time
from time import sleep
from machine import Pin, I2C
import utime

# from stackoverflow J.F. Sebastian
def _twos_comp(val, bits=8):
    """
    compute the 2's complement of int val with bits
    """
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set
        val = val - (1 << bits)        # compute negative value
    return val                         # return positive value as is


class BMA2X2():
    """
    Class for BMA2X2 accelerometer
    """

    def __init__(self, i2c, addr):
        """
        Initializes with an I2C object and address as arguments.
        """

        self.i2c = i2c
        self.acc_addr = addr
        self.chip_id = self.i2c.readfrom_mem(self.acc_addr, 0x00, 1)[0]
        self.set_range(2)      # default range 16g
        self.set_filter_bw(128)    # default filter bandwith 125Hz
        self.compensation()

    def _read_accel(self, addr: int) -> float:
        """
        Returns acceleromter data from address.
        """
        LSB, MSB = self.i2c.readfrom_mem(self.acc_addr, addr, 2)
        LSB = _twos_comp(LSB)
        MSB = _twos_comp(MSB)
        return (LSB + (MSB<<4))*self._resolution/1000

    def temperature(self) -> float:
        """
        Returns temperature in degrees C.
        """
        return self.i2c.readfrom_mem(self.acc_addr, 0x08, 1)[0]/2 + 23

    def set_range(self, accel_range: int):
        """
        Sets the accelerometer range to 2, 4, 8 or 16g.
        """
        ranges = {2:b'\x03', 4:b'\x05', 8:b'\x08', 16:b'\x0C'}
        try:
            range_byte = ranges[accel_range]
        except KeyError:
            raise ValueError('invalid range, use 2, 4, 8 or 16')
        self.i2c.writeto_mem(self.acc_addr, 0x0F, range_byte)
        self._resolution = {2:0.98, 4:1.95, 8:3.91, 16:7.81}[accel_range]

    def get_range(self) -> int:
        """
        Returns the accelerometer range.
        """
        return {3:2,5:4,8:8,12:16}[self.i2c.readfrom_mem(self.acc_addr, 0x0F, 1)[0]]

    def set_filter_bw(self, freq: int):
        """
        Sets the filter bandwith to 8, 16, 32, 64, 128, 256, 512 or 1024Hz.
        """
        freqs = {8:b'\x08', 16:b'\x09', 32:b'\x0A', 64:b'\x0B', 128:b'\x0C', 256:b'\x0D', 512:b'\x0E', 1024:b'\x0F'}
        try:
            freq_byte = freqs[freq]
        except:
            raise ValueError('invalid filter bandwith, use 8, 16, 32, 64, 128, 256, 512 or 1024')
        self.i2c.writeto_mem(self.acc_addr, 0x10, freq_byte)

    def get_filter_bw(self) -> int:
        """
        Returns the filter bandwith.
        """
        return 2**(self.i2c.readfrom_mem(self.acc_addr, 0x10, 1)[0]-5)

    def compensation(self, active=None) -> bool:
        """
        With no arguments passed, runs fast compensation.
        With boolean argument passe, activates or deactivates slow compensation.
        """
        accel_range = self.get_range()
        self.set_range(2)
        self.i2c.writeto_mem(self.acc_addr, 0x37, b'\x21') # settings x0y0z1 10Hz
        self.i2c.writeto_mem(self.acc_addr, 0x36, b'\x80') # reset
        if active is None:  # trigger fast comp
            self.i2c.writeto_mem(self.acc_addr, 0x36, b'\x00') # deactivate slow comp
            active = False
            #print(self.i2c.readfrom_mem(self.acc_addr, 0x36, 1))
            self.i2c.writeto_mem(self.acc_addr, 0x36, b'\x20') # x
            sleep(0.1)
            #print(self.i2c.readfrom_mem(self.acc_addr, 0x36, 1))
            self.i2c.writeto_mem(self.acc_addr, 0x36, b'\x40') # y
            sleep(0.1)
            #print(self.i2c.readfrom_mem(self.acc_addr, 0x36, 1))
            self.i2c.writeto_mem(self.acc_addr, 0x36, b'\x60') # z
            sleep(0.1)
            #print(self.i2c.readfrom_mem(self.acc_addr, 0x36, 1))
        elif active:        # activate slow comp
            self.i2c.writeto_mem(self.acc_addr, 0x36, b'\x07')
        elif not active:    # deactivate slow comp
            self.i2c.writeto_mem(self.acc_addr, 0x36, b'\x00')
        else:
            raise TypeError('pass a boolean or no argument')
        self.set_range(accel_range)
        return active

    def x(self) -> float:
        """
        Returns x acceleration in g.
        """
        return self._read_accel(0x02)

    def y(self) -> float:
        """
        Returns y acceleration in g.
        """
        return self._read_accel(0x04)

    def z(self) -> float:
        """
        Returns z acceleration in g.
        """
        return self._read_accel(0x06)

    def xyz(self) -> tuple:
        """
        Returns x,y and z accelerations in g as tuple.
        """
        return (self.x(), self.y(), self.z())

class Kalman_filter:
    def __init__(self,Q,R):
        self.Q = Q
        self.R = R
        self.P_k_k1 = 1
        self.Kg = 0
        self.P_k1_k1 = 1
        self.x_k_k1 = 0
        self.ADC_OLD_Value = 0
        self.Z_k = 0
        self.kalman_adc_old=0
        
    def kalman(self,ADC_Value):
        self.Z_k = ADC_Value
        if (abs(self.kalman_adc_old-ADC_Value)>=60):
            self.x_k1_k1= ADC_Value*0.400 + self.kalman_adc_old*0.600
        else:
            self.x_k1_k1 = self.kalman_adc_old;
        self.x_k_k1 = self.x_k1_k1
        self.P_k_k1 = self.P_k1_k1 + self.Q
        self.Kg = self.P_k_k1/(self.P_k_k1 + self.R)
        kalman_adc = self.x_k_k1 + self.Kg * (self.Z_k - self.kalman_adc_old)
        self.P_k1_k1 = (1 - self.Kg)*self.P_k_k1
        self.P_k_k1 = self.P_k1_k1
        self.kalman_adc_old = kalman_adc
        return kalman_adc
    
class BMG160():
    """
    Class for BMG160 gyroscope
    """

    def __init__(self, i2c, addr):
        """
        Initializes with an I2C object and address as arguments.
        """

        self.buf = bytearray(64)
        self.i2c = i2c
        self.gyro_addr = addr
        self.chip_id = self.i2c.readfrom_mem(self.gyro_addr, 0x00, 1)[0]
        self.set_range(125)      # default range 2000deg/s
        self.set_filter_bw(116)    # default filter bandwith 64Hz
        self.compensation()

    def _read_gyro(self, addr):
        """
        return gyro data from addr
        """
        LSB, MSB = self.i2c.readfrom_mem(self.gyro_addr, addr, 2)
        LSB = _twos_comp(LSB)
        MSB = _twos_comp(MSB)
        return (LSB + (MSB<<4))*self._resolution

    def set_range(self, gyro_range: int):
        """
        Sets the gyro range to 125, 250, 500, 1000 or 2000deg/s.
        """
        ranges = {125:b'\x04', 250:b'\x03', 500:b'\x02', 1000:b'\x01', 2000:b'\x00'}
        try:
            range_byte = ranges[gyro_range]
        except KeyError:
            raise ValueError('invalid range, use 125, 250, 500, 1000 or 2000')
        self.i2c.writeto_mem(self.gyro_addr, 0x0F, range_byte)
        self._resolution = (2*gyro_range)/2**16

    def get_range(self) -> int:
        """
        Returns the gyro range.
        """
        return int(2000/2**self.i2c.readfrom_mem(self.gyro_addr, 0x0F, 1)[0])

    def set_filter_bw(self, freq: int):
        """
        Sets the filter bandwith to 12, 23, 32, 47, 64, 116, 230 or 523Hz.
        """
        freqs = {12:b'\x05', 23:b'\x04', 32:b'\x07', 47: b'\x03', 64:b'\x06', 116:b'\x02', 230:b'\x01', 523:b'\x00'}
        try:
            freq_byte = freqs[freq]
        except:
            raise ValueError('invalid filter bandwith, use 12, 23, 32, 47, 64, 116, 230 or 523')
        self.i2c.writeto_mem(self.gyro_addr, 0x10, freq_byte)

    def get_filter_bw(self) -> int:
        """
        Returns the filter bandwith.
        """
        return {0:523,1:230,2:116,3:47,4:23,5:12,6:64,7:32}[self.i2c.readfrom_mem(self.gyro_addr, 0x10, 1)[0]]

    def compensation(self, active=None) -> bool:
        """
        With no arguments passed, runs fast compensation.
        With boolean argument passe, activates or deactivates slow compensation.
        """
        gyro_range = self.get_range()
        self.set_range(125)
        self.i2c.writeto_mem(self.gyro_addr, 0x37, b'\x21') # settings x0y0z1 10Hz
        self.i2c.writeto_mem(self.gyro_addr, 0x36, b'\x80') # reset
        if active is None:  # trigger fast comp
            self.i2c.writeto_mem(self.gyro_addr, 0x36, b'\x00') # deactivate slow comp
            active = False
            #print(self.i2c.readfrom_mem(self.gyro_addr, 0x36, 1))
            self.i2c.writeto_mem(self.gyro_addr, 0x36, b'\x20') # x
            sleep(0.1)
            #print(self.i2c.readfrom_mem(self.gyro_addr, 0x36, 1))
            self.i2c.writeto_mem(self.gyro_addr, 0x36, b'\x40') # y
            sleep(0.1)
            #print(self.i2c.readfrom_mem(self.gyro_addr, 0x36, 1))
            self.i2c.writeto_mem(self.gyro_addr, 0x36, b'\x60') # z
            sleep(0.1)
            #print(self.i2c.readfrom_mem(self.gyro_addr, 0x36, 1))
        elif active:        # activate slow comp
            self.i2c.writeto_mem(self.gyro_addr, 0x36, b'\x07')
        elif not active:    # deactivate slow comp
            self.i2c.writeto_mem(self.gyro_addr, 0x36, b'\x00')
        else:
            raise TypeError('pass a boolean or no argument')
        self.set_range(gyro_range)
        return active

    def x(self) -> float:
        return self._read_gyro(0x02)

    def y(self) -> float:
        return self._read_gyro(0x04)

    def z(self) -> float:
        return self._read_gyro(0x06)

    def xyz(self) -> tuple:
        return (self.x(), self.y(), self.z())

class IMU:
    def __init__(self, accel, gyro):
        self.Kp = 100 
        self.Ki = 0.002 
        self.halfT = 0.001 

        self.q0 = 1
        self.q1 = 0
        self.q2 = 0
        self.q3 = 0

        self.exInt = 0
        self.eyInt = 0
        self.ezInt = 0
        self.pitch = 0
        self.roll =0
        self.yaw = 0

        self.accel = accel
        self.gyro = gyro
        
        self.kalman_filter_AX =  Kalman_filter(0.001,0.1)
        self.kalman_filter_AY =  Kalman_filter(0.001,0.1)
        self.kalman_filter_AZ =  Kalman_filter(0.001,0.1)

        self.kalman_filter_GX =  Kalman_filter(0.001,0.1)
        self.kalman_filter_GY =  Kalman_filter(0.001,0.1)
        self.kalman_filter_GZ =  Kalman_filter(0.001,0.1)
        
        self.Error_value_accel_data,self.Error_value_gyro_data=self.average_filter()
    
    def average_filter(self):
        sum_accel_x=0
        sum_accel_y=0
        sum_accel_z=0
        
        sum_gyro_x=0
        sum_gyro_y=0
        sum_gyro_z=0
        for i in range(100):  
            sum_accel_x+=self.accel.x()
            sum_accel_y+=self.accel.y()
            sum_accel_z+=self.accel.z()
            
            sum_gyro_x+=self.gyro.x()
            sum_gyro_y+=self.gyro.y()
            sum_gyro_z+=self.gyro.z()
            
        sum_accel_x/=100
        sum_accel_y/=100
        sum_accel_z/=100
        
        sum_gyro_x/=100
        sum_gyro_y/=100
        sum_gyro_z/=100
        
        accel_data = {'x': sum_accel_x, 'y': sum_accel_y, 'z': sum_accel_z-9.8}
        gyro_data  = {'x': sum_gyro_x, 'y': sum_gyro_y, 'z': sum_gyro_z}
        
        return accel_data, gyro_data
    
    def imuUpdate(self):
        ax=self.kalman_filter_AX.kalman(self.accel.x()-self.Error_value_accel_data['x'])
        ay=self.kalman_filter_AY.kalman(self.accel.y()-self.Error_value_accel_data['y'])
        az=self.kalman_filter_AZ.kalman(self.accel.z()-self.Error_value_accel_data['z'])
        gx=self.kalman_filter_GX.kalman(self.gyro.x()-self.Error_value_gyro_data['x'])
        gy=self.kalman_filter_GY.kalman(self.gyro.y()-self.Error_value_gyro_data['y'])
        gz=self.kalman_filter_GZ.kalman(self.gyro.z()-self.Error_value_gyro_data['z'])

        norm = math.sqrt(ax*ax+ay*ay+az*az)
        
        ax = ax/norm
        ay = ay/norm
        az = az/norm
        
        vx = 2*(self.q1*self.q3 - self.q0*self.q2)
        vy = 2*(self.q0*self.q1 + self.q2*self.q3)
        vz = self.q0*self.q0 - self.q1*self.q1 - self.q2*self.q2 + self.q3*self.q3
        
        ex = (ay*vz - az*vy)
        ey = (az*vx - ax*vz)
        ez = (ax*vy - ay*vx)
        
        self.exInt += ex*self.Ki
        self.eyInt += ey*self.Ki
        self.ezInt += ez*self.Ki
        
        gx += self.Kp*ex + self.exInt
        gy += self.Kp*ey + self.eyInt
        gz += self.Kp*ez + self.ezInt
        
        self.q0 += (-self.q1*gx - self.q2*gy - self.q3*gz)*self.halfT
        self.q1 += (self.q0*gx + self.q2*gz - self.q3*gy)*self.halfT
        self.q2 += (self.q0*gy - self.q1*gz + self.q3*gx)*self.halfT
        self.q3 += (self.q0*gz + self.q1*gy - self.q2*gx)*self.halfT
        
        norm = math.sqrt(self.q0*self.q0 + self.q1*self.q1 + self.q2*self.q2 + self.q3*self.q3)
        self.q0 /= norm
        self.q1 /= norm
        self.q2 /= norm
        self.q3 /= norm
        
        pitch = math.asin(-2*self.q1*self.q3+2*self.q0*self.q2)*57.3
        roll = math.atan2(2*self.q2*self.q3+2*self.q0*self.q1,-2*self.q1*self.q1-2*self.q2*self.q2+1)*57.3
        yaw = math.atan2(2*(self.q1*self.q2 + self.q0*self.q3),self.q0*self.q0+self.q1*self.q1-self.q2*self.q2-self.q3*self.q3)*57.3
        self.roll = roll*14.3
        self.pitch = pitch*14.3
        self.yaw = yaw
        return self.roll, self.pitch, self.yaw


i2c_sda = Pin(4)
i2c_scl = Pin(5)

i2c = I2C(0,sda=i2c_sda,scl=i2c_scl,freq=100000)

addresses = [ int(address) for address in i2c.scan() ]

print('Found devices on', addresses)

utime.sleep_ms(100)

accel = BMA2X2(i2c, 24)
gyro = BMG160(i2c, 104)
imu = IMU(accel, gyro)

while True:
    print(imu.imuUpdate())
