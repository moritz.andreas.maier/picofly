from machine import Pin,PWM
from time import sleep

# PicoFly PWM Pins
H1_PWM = 18
H2_PWM = 19
H3_PWM = 20
H4_PWM = 21

servo = PWM(Pin(18, mode=Pin.OUT))
servo.freq(50)
pause = 0.05

# Zero position
servo.duty_u16(5000)
sleep(3)

# Max. range 1000 to 9000
while True:
    for position in range(1000,9000,50):
        servo.duty_u16(position)
        print(position)
        sleep(0.01)
    for position in range(9000,1000,-50):
        servo.duty_u16(position)
        print(position)
        sleep(0.01)
