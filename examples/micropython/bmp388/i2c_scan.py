from machine import Pin, I2C

i2c_sda = Pin(4)
i2c_scl = Pin(5)

i2c = I2C(0,sda=i2c_sda,scl=i2c_scl,freq=100000)

print('Scanning I2C bus...')
devices = i2c.scan()

if len(devices) == 0:
    print('No I2C device found!')
else:
    print('I2C devices found:', len(devices))
    for device in devices:
        print('Decimal address:', device, '| Hexadecimal address:', hex(device))
