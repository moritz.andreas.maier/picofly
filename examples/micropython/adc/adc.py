import machine
import time

adc = machine.ADC(26)

# voltage scaling factor
s = 0.0001051

while True:
    print(adc.read_u16()*s)
    time.sleep(0.5)