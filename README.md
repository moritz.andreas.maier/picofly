# PicoFly

A custom development board for Raspberry Pi Pico and Raspberry Pi Pico W.

![PicoFly v1.0](./docu/PCB_3D_2023-03-22.png "PicoFly v1.0")

## It contains 

- BMX055 9DoF IMU
- BMP388 barometric pressure sensor
- WS2813B RGB LED
- 4x LB1938FA DC motor drivers with Molex Picoblade connectors
- 2 pin single cell LiPo connector
- 2 pin buzzer connector
- 4x PWM / servo pin header
- I2C Grove connector pin header
- Custom SPI / GPIO pin header

## I2C adresses

| Device | Dec. | Hex. |
|:---|:--:|:--:|
| Accelerometer | 24 | 0x18 |
| Gyroscope | 104 | 0x68 |
| Magnetometer | 16 | 0x10 |
| Barometer | 118 | 0x76 |

## Recommended IDEs

- [Visual Studio Code](https://code.visualstudio.com/) with [Pico-W-Go Extension](https://marketplace.visualstudio.com/items?itemName=paulober.pico-w-go)
- [Visual Studio Code](https://code.visualstudio.com/) with [platformIO Extension](https://marketplace.visualstudio.com/items?itemName=platformio.platformio-ide)
- [Thonny IDE](https://thonny.org/)
- [Arduino IDE](https://www.arduino.cc/en/software) with [Raspberry Pi Pico Arduino core](https://github.com/earlephilhower/arduino-pico)

## Getting started with Micropython

- Download latest firmware from https://micropython.org/download/rp2-pico-w
- Hold the BOOTSEL button while connecting Pico W via USB
- Using a file explorer of your choice, drag and drop the firmware file onto the Pico W

## What's in the box

![PicoFly box content](./docu/230422_picofly_contents.jpg "Box content")

## Getting started

Solder the Raspberry Pi Pico W directly onto the PicoFly using the provided pins as shown below. If you want the Pico W to be removable, cut and solder the provided pin sockets onto the PicoFly.

![PicoFly assembled](./docu/230422_picofly_assembled.jpg "PicoFly assembled")

## ADC battery monitoring fix

In the PicoFly v1.0 design, I accidentally connected the voltage divider for battery monitoring to ADC_VREF instead of a proper ADC pin. This can be fixed by connecting ADC_VREF with GP26 (XSHUT) on the PicoFly using the provided gray jumper wire. On Pico W, leave ADC_VREF unconnected. Also, leave one GND pin unconnected as shown above to create space for the gray jumper wire to get through. 

![PicoFly ADC fix](./docu/PCB_3D_2023-03-22_fix.png "PicoFly ADC fix")

## Examples

- [Micropython](./examples/micropython)
- [C++ / PlatformIO](./examples/cpp)

## Projects

- [Remote-controlled 4-wheel-drive car](./projects/rc_car_4wd)
- [Remote-controlled servo steering car](./projects/rc_car_servo)
- [PicoFly GUI for RC cars](./projects/picofly_gui)
- [PicoFly quadcopter](./projects/quadcopter)
