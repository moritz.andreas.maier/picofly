# Remote-controlled servo steering car

![RC car servo](./rc_car_servo.jpg "RC car servo")

## Getting started

- Save [main.py](./main.py) to Raspberry Pi Pico W, for example using the [Thonny IDE](https://thonny.org)
- Make sure to add your WiFi name and password at ``` wifi_name = '<WiFi name>' ``` and ``` wifi_pswd = '<WiFi password>' ```, respectively
- To control the car use the [PicoFly GUI](../picofly_gui) and a USB gamepad
