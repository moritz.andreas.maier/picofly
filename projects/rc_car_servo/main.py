import network
import socket
import time
from time import sleep
import json
import neopixel
from machine import Pin, PWM, ADC

# Parameters
wifi_name = '<WiFi name>'
wifi_pswd = '<WiFi password>'
max_speed = 60 # 0 to 100
max_steer = 800 # 0 to 4000
servo_zero = 5080 # between 0 and 9000
enable_led = False
enable_buzzer = False
voltage_min = 3.7
voltage_scale = 0.0001051 # scaling factor

# ADC for battery monitoring
adc = ADC(26)

# PicoFly PWM Pins
H1_PWM = 18
#H2_PWM = 19
#H3_PWM = 20
#H4_PWM = 21

# Servo
servo = PWM(Pin(H1_PWM, mode=Pin.OUT))
servo.freq(50)
servo.duty_u16(servo_zero)

# Servo test
servo.duty_u16(servo_zero+max_steer)
time.sleep(0.1)
servo.duty_u16(servo_zero)
time.sleep(0.1)
servo.duty_u16(servo_zero-max_steer)
time.sleep(0.1)
servo.duty_u16(servo_zero)

# LED
if enable_led:
  led_pin = Pin(28)
  np = neopixel.NeoPixel(led_pin, 1)
  np[0] = (255, 0, 0) # red
  np.write()
  sleep(1)
  np[0] = (0, 255, 0) # green
  np.write()
  sleep(1)
  np[0] = (0, 0, 255) # blue
  np.write()
  sleep(1)
  np[0] = (0, 0, 0) # off
  np.write()

# Motors
motor1B = PWM(Pin(1, mode=Pin.OUT))
#motor2B = PWM(Pin(3, mode=Pin.OUT))
#motor3B = PWM(Pin(6, mode=Pin.OUT))
motor4B = PWM(Pin(8, mode=Pin.OUT))
motor1F = PWM(Pin(0, mode=Pin.OUT))
#motor2F = PWM(Pin(2, mode=Pin.OUT))
#motor3F = PWM(Pin(7, mode=Pin.OUT))
motor4F = PWM(Pin(9, mode=Pin.OUT))

#motors = [motor1F, motor2F, motor3F, motor4F, motor1B, motor2B, motor3B, motor4B]
motors = [motor1F, motor4F, motor1B, motor4B]
for motor in motors:
    motor.freq(15600)

# Stop all motors
motor1F.duty_u16(int(0))
#motor2F.duty_u16(int(0))
#motor3F.duty_u16(int(0))
motor4F.duty_u16(int(0))
motor1B.duty_u16(int(0))
#motor2B.duty_u16(int(0))
#motor3B.duty_u16(int(0))
motor4B.duty_u16(int(0))

# WiFi
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(wifi_name, wifi_pswd)
port = 30042

while(wlan.isconnected() == False):
    time.sleep(1)
ip = wlan.ifconfig()[0]
print("Pico W IP: " + ip)

# UDP
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1) 
s.bind((ip, port))
print('Waiting for data ...')

# Horn
buzzerPinNum=16
buzzerPin = Pin(buzzerPinNum)

if enable_buzzer:
  BuzzerObj = PWM(buzzerPin)

def buzzer(buzzerPinObject,frequency,sound_duration,silence_duration):
    # Set duty cycle to a positive value to emit sound from buzzer
    buzzerPinObject.duty_u16(int(65536*0.1))
    # Set frequency
    buzzerPinObject.freq(frequency)
    # Wait for sound duration
    sleep(sound_duration)
    # Set duty cycle to zero to stop sound
    buzzerPinObject.duty_u16(int(65536*0))
    # Wait for sound interrumption, if needed 
    sleep(silence_duration)

while True:
  # UDP
  data,addr = s.recvfrom(1024)
  joystick = json.loads(data)
  
  # Update control parameters
  max_speed = joystick['throttle']['max']
  max_steer = joystick['steering']['max']
  
  speed = (-1)*joystick['axes'][joystick['throttle']['axis']]*max_speed
  steer = (1)*joystick['axes'][joystick['steering']['axis']]*max_steer + servo_zero

  print("Speed: " + str(speed) + ", Steer: " + str(int(steer)))

  # Set servo position
  servo.duty_u16(int(steer))
  time.sleep(1.0/joystick['rate'])

  # Set motor speeds
  if speed >= 0:
    motor1F.duty_u16(int(speed/100*65536))
    #motor2F.duty_u16(int(speed/100*65536))
    motor1B.duty_u16(int(0))
    #motor2B.duty_u16(int(0))
    #motor3F.duty_u16(int(speed/100*65536))
    motor4F.duty_u16(int(speed/100*65536))
    #motor3B.duty_u16(int(0))
    motor4B.duty_u16(int(0))
  else:
    motor1F.duty_u16(int(0))
    #motor2F.duty_u16(int(0))
    motor1B.duty_u16(int((-1)*speed/100*65536))
    #motor2B.duty_u16(int((-1)*speed/100*65536))
    #motor3F.duty_u16(int(0))
    motor4F.duty_u16(int(0))
    #motor3B.duty_u16(int((-1)*speed/100*65536))
    motor4B.duty_u16(int((-1)*speed/100*65536))
  
  if joystick['led']['enable'] > 0:
    enable_led = True
  else:
    enable_led = False
  
  if enable_led:
      led_pin = Pin(28)
      np = neopixel.NeoPixel(led_pin, 1)
      np[0] = (joystick['led']['red'], joystick['led']['green'], joystick['led']['blue'])
      np.write()
  else:
      led_pin = Pin(28)
      np = neopixel.NeoPixel(led_pin, 1)
      np[0] = (0, 0, 0) # off
      np.write()
  
  if joystick['buzzer']['enable'] > 0:
    enable_buzzer = True
  else:
    enable_buzzer = False
    buzzerPin.off()

  if enable_buzzer:
    if joystick['buttons'][joystick['buzzer']['button']]:
      BuzzerObj = PWM(buzzerPin)
      do5=523
      buzzer(BuzzerObj,do5,0.1,0.1)
      BuzzerObj.deinit()
    else:
      buzzerPin.off()
  else:
    buzzerPin.off()
      
  if adc.read_u16()*voltage_scale < voltage_min:
      BuzzerObj = PWM(buzzerPin)
      mi5=659
      buzzer(BuzzerObj,mi5,0.1,0.1)
      BuzzerObj.deinit()
