# moritz.andreas.maier@gmail.com
# 
# INSTALLATION
# python -m pip install -r requirements.txt 
#
# START
# python picofly_gui.py
# 
# CREATE EXE:
# pyinstaller --onefile --windowed --add-data "picofly_gui.ui:." picofly_gui.py
# pipreqs ../picofly_gui
# BUILD Windows EXE:
# https://www.makeworld.space/2021/10/linux-wine-pyinstaller.html
# wine C:/Python38/python.exe -m pip install -r requirements.txt
# wine C:/Python38/python.exe -m pip install pyinstaller
# wine C:/Python38/Scripts/pyinstaller.exe --onefile --windowed --add-data "picofly_gui.ui:." picofly_gui.py

from PyQt5 import QtWidgets, QtGui, uic
from PyQt5.QtCore import QTimer, QDateTime, Qt, pyqtSlot
from threading import Thread
from PyQt5.QtGui import QPixmap, QIcon
import sys
import pygame
import numpy as np
import socket
import json

pygame.init()

class Gamepad(Thread):
    
    def __init__(self):
        Thread.__init__(self)
        self.ip = ""
        self.stop = False
        self.rate = 50

        pygame.display.set_mode((200, 100))
        pygame.display.set_caption("Gamepad")

        self.clock = pygame.time.Clock()
        self.joysticks = {}
        self.joystick_count = pygame.joystick.get_count()
        print(f"Number of joysticks: {self.joystick_count}")

        self.reset_lists()
    
    def reset_lists(self):
        self.axes_list = []
        self.buttons_list = [] 
        self.hats_list = []

    def set_rate(self, rate):
        self.rate = rate
        
    def run(self):
        while (self.stop == False):
            # Event processing step.
            # Possible joystick events: JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONDOWN,
            # JOYBUTTONUP, JOYHATMOTION, JOYDEVICEADDED, JOYDEVICEREMOVED
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True  # Flag that we are done so we exit this loop.

                if event.type == pygame.JOYBUTTONDOWN:
                    print("Joystick button pressed.")
                    #if event.button == 0:
                        #joystick = self.joysticks[event.instance_id]
                        #if joystick.rumble(0, 0.7, 500):
                        #    print(f"Rumble effect played on joystick {event.instance_id}")

                if event.type == pygame.JOYBUTTONUP:
                    print("Joystick button released.")

                # Handle hotplugging
                if event.type == pygame.JOYDEVICEADDED:
                    # This event will be generated when the program starts for every
                    # joystick, filling up the list without needing to create them manually.
                    joy = pygame.joystick.Joystick(event.device_index)
                    self.joysticks[joy.get_instance_id()] = joy
                    print(f"Joystick {joy.get_instance_id()} connected")

                if event.type == pygame.JOYDEVICEREMOVED:
                    del self.joysticks[event.instance_id]
                    print(f"Joystick {event.instance_id} disconnected")
            
            for joystick in self.joysticks.values():
                self.reset_lists()

                for i in range(joystick.get_numaxes()):
                    self.axes_list.append(joystick.get_axis(i))
                
                for i in range(joystick.get_numbuttons()):
                    self.buttons_list.append(joystick.get_button(i))
                
                for i in range(joystick.get_numhats()):
                    self.hats_list.append(joystick.get_hat(i))

            pygame.display.flip()
            self.clock.tick(self.rate)
        
        pygame.quit()

    def stop(self):
        self.stop = True


class MainWindow(QtWidgets.QDialog):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        # Load the UI
        uic.loadUi('picofly_gui.ui', self)

        # Gamepad thread
        self.gamepad = Gamepad()
        self.gamepad.start()

        self.gui_update_timer=QTimer(self)
        self.gui_update_timer.timeout.connect(self.gui_update)
        self.gui_update_timer.start(100) # default 10 Hz

        self.closeButton.pressed.connect(self.close)

        # UDP
        self.rate = 50
        self.udp_ip = "192.168.178.128"
        self.udp_port = 30042
        self.udpAddressPort = (self.udp_ip, self.udp_ip)
        self.udpClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

    def close(self):
        pygame.quit()
        sys.exit(0)

    def gui_update(self):
        self.textBrowser.clear()
        for joystick in self.gamepad.joysticks.values():
            self.textBrowser.append(f"Joystick {joystick.get_instance_id()}")
            self.textBrowser.append(f"Joystick name: {joystick.get_name()}")
            self.textBrowser.append(f"GUID: {joystick.get_guid()}")
            self.textBrowser.append((f"Number of axes: {joystick.get_numaxes()}"))
            self.textBrowser.append(f"Number of buttons: {joystick.get_numbuttons()}")
            self.textBrowser.append(f"Number of hats: {joystick.get_numhats()}")
            self.textBrowser.append(f"Joystick's power level: {joystick.get_power_level()}")
            self.textBrowser.append("Axes: " + str(np.round(self.gamepad.axes_list,3)))
            self.textBrowser.append("Buttons: " + str(self.gamepad.buttons_list))
            self.textBrowser.append("Hats: " + str(self.gamepad.hats_list))
            self.textBrowser.append("<b>Click on black Gamepad window to activate!</b>")
        
        self.textBrowserMsg.clear()
        self.rate = self.spinBoxRate.value()
        self.gamepad.set_rate(self.rate)
        self.gui_update_timer.setInterval(int(1.0/self.rate)*1000)

        # JSON string
        joystick_msg = json.dumps({'axes':  self.gamepad.axes_list, 'buttons': self.gamepad.buttons_list, 'hats': self.gamepad.hats_list, 
                                   'led': {'enable': self.checkBoxLED.checkState(),
                                           'red': self.spinBoxLEDRed.value(), 
                                           'green': self.spinBoxLEDGreen.value(),
                                           'blue': self.spinBoxLEDBlue.value()},
                                   'buzzer': {'enable': self.checkBoxBuzzer.checkState(), 
                                              'button': self.spinBoxBuzzerButton.value()},
                                   'throttle': {'axis': self.spinBoxThrottleAxis.value(), 
                                                 'max': self.spinBoxThrottleMax.value()},
                                   'steering': {'axis': self.spinBoxSteerAxis.value(), 
                                                 'max': self.spinBoxSteerMax.value()}, 
                                   'rate': self.spinBoxRate.value()})
        self.textBrowserMsg.append(joystick_msg)

        # Update IP
        self.udp_ip = self.lineEditIP.text()
        self.udpAddressPort = (self.udp_ip, self.udp_port)

        # Send UDP packet
        self.udpClientSocket.sendto(joystick_msg.encode('utf-8'), self.udpAddressPort)

def main():
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
    pygame.quit()