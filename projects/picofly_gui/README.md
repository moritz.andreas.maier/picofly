# PicoFly GUI for RC cars

![PicoFly GUI](./picofly_gui.png "PicoFly GUI")

## To run the GUI

- Install python from [www.python.org](https://www.python.org/downloads)
- ``` python -m pip install -r requirements.txt ```
- In the directory where ``` picofly_gui.ui ``` is located run ``` python picofly_gui.py ```
