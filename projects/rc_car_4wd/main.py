import network
import socket
import time
from time import sleep
import json
import neopixel
from machine import Pin, PWM

# Parameters
wifi_name = '<WiFi name>'
wifi_pswd = '<WiFi password>'
max_speed = 60
max_steer = 40
enable_buzzer = False

# LED
# led_pin = Pin(28)
# np = neopixel.NeoPixel(led_pin, 1)
# np[0] = (255, 0, 0) # red
# np.write()
# sleep(1)
# np[0] = (0, 255, 0) # green
# np.write()
# sleep(1)
# np[0] = (0, 0, 255) # blue
# np.write()
# sleep(1)
# np[0] = (0, 0, 0) # off
# np.write()

# Motors
motor1F = PWM(Pin(1, mode=Pin.OUT))
motor2F = PWM(Pin(3, mode=Pin.OUT))
motor3F = PWM(Pin(6, mode=Pin.OUT))
motor4F = PWM(Pin(8, mode=Pin.OUT))
motor1B = PWM(Pin(0, mode=Pin.OUT))
motor2B = PWM(Pin(2, mode=Pin.OUT))
motor3B = PWM(Pin(7, mode=Pin.OUT))
motor4B = PWM(Pin(9, mode=Pin.OUT))

motors = [motor1F, motor2F, motor3F, motor4F, motor1B, motor2B, motor3B, motor4B]
for motor in motors:
    motor.freq(15600)

# WiFi
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(wifi_name, wifi_pswd)
port = 30042

while(wlan.isconnected() == False):
    time.sleep(1)
ip = wlan.ifconfig()[0]
print("Pico W IP: " + ip)

# UDP
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((ip, port))
print('Waiting for data ...')

# Stop all motors
motor1F.duty_u16(int(0))
motor2F.duty_u16(int(0))
motor3F.duty_u16(int(0))
motor4F.duty_u16(int(0))
motor1B.duty_u16(int(0))
motor2B.duty_u16(int(0))
motor3B.duty_u16(int(0))
motor4B.duty_u16(int(0))

# Horn
buzzerPIN=16
if enable_buzzer:
  BuzzerObj = PWM(Pin(buzzerPIN))

def buzzer(buzzerPinObject,frequency,sound_duration,silence_duration):
    # Set duty cycle to a positive value to emit sound from buzzer
    buzzerPinObject.duty_u16(int(65536*0.1))
    # Set frequency
    buzzerPinObject.freq(frequency)
    # wait for sound duration
    sleep(sound_duration)
    # Set duty cycle to zero to stop sound
    buzzerPinObject.duty_u16(int(65536*0))
    # Wait for sound interrumption, if needed 
    sleep(silence_duration)

while True:
  # UDP
  data,addr = s.recvfrom(1024)
  joystick = json.loads(data)
  
  speed = (-1)*joystick['axes'][3]*max_speed
  steer = (1)*joystick['axes'][2*0]*max_steer

  print("Speed: " + str(speed) + ", Steer: " + str(steer))
  
  if speed-steer >= 0:
    motor1F.duty_u16(int((speed-steer)/100*65536))
    motor2F.duty_u16(int((speed-steer)/100*65536))
    motor1B.duty_u16(int(0))
    motor2B.duty_u16(int(0))
  else:
    motor1F.duty_u16(int(0))
    motor2F.duty_u16(int(0))
    motor1B.duty_u16(int((-1)*(speed-steer)/100*65536))
    motor2B.duty_u16(int((-1)*(speed-steer)/100*65536))
  if speed+steer >= 0:
    motor3F.duty_u16(int((speed+steer)/100*65536))
    motor4F.duty_u16(int((speed+steer)/100*65536))
    motor3B.duty_u16(int(0))
    motor4B.duty_u16(int(0))
  else:
    motor3F.duty_u16(int(0))
    motor4F.duty_u16(int(0))
    motor3B.duty_u16(int((-1)*(speed+steer)/100*65536))
    motor4B.duty_u16(int((-1)*(speed+steer)/100*65536))
    
  if enable_buzzer:
    if joystick['buttons'][8]:
      BuzzerObj = PWM(Pin(buzzerPIN))
      mi5=659
      buzzer(BuzzerObj,mi5,0.1,0.1)
      BuzzerObj.deinit()
