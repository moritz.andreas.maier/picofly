# Remote-controlled 4-wheel-drive car

![RC car 4wd](./rc_car_4wd.jpg "RC car 4wd")

## Getting started

- Save [main.py](./main.py) to Raspberry Pi Pico W, for example using the [Thonny IDE](https://thonny.org)
- Make sure to add your WiFi name and password at ``` wifi_name = '<WiFi name>' ``` and ``` wifi_pswd = '<WiFi password>' ```, respectively
- To control the car use the [PicoFly GUI](../picofly_gui) and a USB gamepad
